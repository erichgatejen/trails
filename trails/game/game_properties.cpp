/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: game
 * \file game_properties.cpp
 *
 * Game properties implementations.
 *
 * \author Erich Gatejen
 */

#include "game/game_properties.h"

const trails::base::trails_property_name trails::game::PROP__NAME    = "name";
const trails::base::trails_property_name trails::game::PROP__VERSION = "version";

const std::vector<trails::base::trails_property_name> trails::game::project_properties = {
    trails::game::PROP__NAME,
    trails::game::PROP__VERSION
};

void trails::game::trails_load_default_project_properties(trails::base::Properties & props)
{
    props[trails::game::PROP__NAME] = {trails::game::PROP__NAME, "New game"};
    props[trails::game::PROP__VERSION] = {trails::game::PROP__VERSION, "1"};
}



