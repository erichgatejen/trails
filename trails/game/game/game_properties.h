/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: game
 * \file game_properties.hpp
 *
 * Game specific properties.
 *
 * \author Erich Gatejen
 */

#pragma once
#ifndef __GAME_GAME_PROPERTIES__
#define __GAME_GAME_PROPERTIES__

// ===================================================================================================================
// INCLUDES
//

#include <string>
#include <variant>
#include <map>

#include <cereal/archives/portable_binary.hpp>
#include <cereal/types/variant.hpp>

#include "base/base.hpp"
#include "base/properties.hpp"

namespace trails::game {

// ===================================================================================================================
// PROPERTIES
//

extern const trails::base::trails_property_name PROP__NAME;
extern const trails::base::trails_property_name PROP__VERSION;
extern const std::vector<trails::base::trails_property_name> project_properties;

void trails_load_default_project_properties(trails::base::Properties & props);

// ===================================================================================================================
// HELPERS
//



} // namespace trails::game



#endif  // __GAME_GAME_PROPERTIES__

