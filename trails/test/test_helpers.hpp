#pragma once

#include "basesys/data.hpp"

#define CHEAP_LOGGER auto sink = std::make_shared<gsys::ConsumerSinkSimpleFIFO<gsys::LoggerRecord>>(); \
    std::shared_ptr<gsys::Logger> logger = std::make_shared<gsys::Logger>(sink, 1);