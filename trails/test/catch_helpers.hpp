#pragma once

#define WRAP_TRY try {
#define WRAP_CATCH } catch (BaseException &b) { FAIL(b.render()); } catch (std::exception &e) { FAIL("Spurious Exception"); }

#define WRAP_IN_BASEEXCEPTION_FAILURE(OPERATION) WRAP_TRY OPERATION WRAP_CATCH

#define DEFINE_TEST_STRINGSOURCE(STRING) CHARSOURCE_DECLARE = CHARSOURCE_INSTANCE__STRING(STRING);

// These are probably somewhat slow.
#define TD_START_DATA(SIZE) CSTRING _td__string_; _td__string_.reserve(SIZE);
#define TD_ADD_CHAR_XTIMES(CHAR,XTIMES) _td__string_.append(XTIMES,CHAR);
#define TD_ADD_STRING(S) _td__string_.append(S);
#define TD_STRING_VAR _td__string_


#define TDS_START_SCANNER(STR) auto _tds__target_ = &STR; int _tds__spot_ = 0;
#define TDS_NEXT _tds__spot_++;
#define TDS_CHECK(CHR) if (_tds__spot_ >= _tds__target_->size()) { FAIL("Scanned passed end of string."); }  REQUIRE(CHR == _tds__target_->at(_tds__spot_));
#define TDS_NEXT_AND_CHECK(CHR) TDS_NEXT TDS_CHECK(CHR)
#define TDS_SKIP(I) _tds__spot_ += I;

#define REQUIRE_FOUND(STR,VALUE) REQUIRE(STR.find(VALUE) != std::string::npos);
#define REQUIRE_NOT_FOUND(STR,VALUE) REQUIRE(STR.find(VALUE) == std::string::npos);


