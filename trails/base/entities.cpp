/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: base
 * \file entities.cpp
 *
 * Base implementations.
 *
 * \author Erich Gatejen
 */

#include "base/entities.hpp"

// ===================================================================================================================
// DATA
//

std::atomic<trails::base::entity_id> id_getter__workflow_node(trails::base::starting_id);

// ===================================================================================================================
// ENTITIES
//

trails::base::WorkflowNode_Demonstration::WorkflowNode_Demonstration() :
    trails::base::WorkflowNodeBase(trails::base::WorkflowNodeType::DEMONSTRATION)
{
}

trails::base::WorkflowNode_Demonstration::WorkflowNode_Demonstration(const entity_id id) :
    trails::base::WorkflowNodeBase(id, trails::base::WorkflowNodeType::DEMONSTRATION)
{
}

trails::base::WorkflowNode_Choice::WorkflowNode_Choice() :
    trails::base::WorkflowNodeBase(trails::base::WorkflowNodeType::CHOICE)
{
}

trails::base::WorkflowNode_Choice::WorkflowNode_Choice(const entity_id id) :
    trails::base::WorkflowNodeBase(id, trails::base::WorkflowNodeType::CHOICE)
{
}

trails::base::WorkflowNode_Composite::WorkflowNode_Composite() :
    trails::base::WorkflowNodeBase(trails::base::WorkflowNodeType::COMPOSITE)
{
}
trails::base::WorkflowNode_Composite::WorkflowNode_Composite(const entity_id id) :
    trails::base::WorkflowNodeBase(id, trails::base::WorkflowNodeType::COMPOSITE)
{
}

