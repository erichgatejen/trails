/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: base
 * \file entities.hpp
 *
 * Base include.
 *
 * \author Erich Gatejen
 */

#pragma once
#ifndef __BASE_ENTITIES__
#define __BASE_ENTITIES__

// ===================================================================================================================
// INCLUDES
//

#include <string>
#include <atomic>
#include <chrono>

#include <cereal/archives/portable_binary.hpp>
#include <cereal/types/variant.hpp>

#include "base.hpp"


namespace trails::base {

// ===================================================================================================================
// HINTS
//

using hints = u_int32_t;
enum class Hints
{
    RENDER_NORTH = 0b00000001,
    RENDER_SOUTH = 0b00000010,
    RENDER_EAST  = 0b00000100,
    RENDER_WEST =  0b00001000
};

// ===================================================================================================================
// WORKFLOW NODES
//

using entity_id = trails::base::trails_id;
extern std::atomic<entity_id> id_getter__workflow_node;

enum class WorkflowNodeType
{
    NOOP = 0,
    DEMONSTRATION = 1,
    CHOICE = 2,
    COMPOSITE = 3
};

class WorkflowNodeBase
{
    C_FRIEND;
    hints     hints_;
    entity_id id_;
    WorkflowNodeType type_; ///! why not let the varient handle it?   We are serialising these.

public:
    C_SERIALIZE(id_, type_);

    WorkflowNodeBase(const WorkflowNodeType type)
    {
        id_ = id_getter__workflow_node.fetch_add(1, std::memory_order_relaxed);
        type_ = type;
    }

    WorkflowNodeBase(const entity_id id, WorkflowNodeType type)
    {
        id_ = id;
        type_ = type;
    }

    virtual ~WorkflowNodeBase() = default;

    inline entity_id get_id() const {
        return id_;
    }

    inline WorkflowNodeType get_type() const {
        return type_;
    }

    inline void hints(trails::base::Hints hint) { hints_ += static_cast<trails::base::hints>(hint); }
    inline void hints(trails::base::hints hint) { hints_ += hint; }
};

class WorkflowNode_Demonstration : public WorkflowNodeBase
{
    C_FRIEND;
    std::chrono::milliseconds   display_time_in_millis_;

public:
    C_SERIALIZE(display_time_in_millis_);

    WorkflowNode_Demonstration();
    WorkflowNode_Demonstration(const entity_id id);
    virtual ~WorkflowNode_Demonstration() = default;

    inline const std::chrono::milliseconds get_display_time_in_millis() const { return display_time_in_millis_; }
    inline void set_display_time_in_millis(const std::chrono::milliseconds display_time_in_millis) { display_time_in_millis_ = display_time_in_millis; }
};

class WorkflowNode_Choice : public WorkflowNodeBase
{
    C_FRIEND;
    std::vector<std::string> choices_;

public:
    C_SERIALIZE(choices_);

    WorkflowNode_Choice();
    WorkflowNode_Choice(const entity_id id);
    virtual ~WorkflowNode_Choice() = default;

    inline void add_choice(const std::string choice) { choices_.push_back(choice); }
    inline std::vector<std::string> & get_choices() { return choices_; }
};

class WorkflowNode_Composite;
using WorkflowNode = std::variant<trails::base::WorkflowNode_Demonstration, trails::base::WorkflowNode_Choice,
                                  trails::base::WorkflowNode_Composite>;

class WorkflowNode_Composite : public WorkflowNodeBase
{
    C_FRIEND;
    std::shared_ptr<trails::base::WorkflowNode> master_;
    std::vector<std::shared_ptr<trails::base::WorkflowNode>> subordinates_;

public:
    C_SERIALIZE(master_, subordinates_);

    WorkflowNode_Composite ();
    WorkflowNode_Composite(const entity_id id);
    virtual ~WorkflowNode_Composite() = default;

    inline std::shared_ptr<trails::base::WorkflowNode> get_master() { return master_; }
    inline void set_master(std::shared_ptr<trails::base::WorkflowNode> master) { master_ = master; }
    inline std::vector<std::shared_ptr<trails::base::WorkflowNode>> & get_subordinates() { return subordinates_; }
    inline void add_subordinate(std::shared_ptr<trails::base::WorkflowNode> subordinate) { subordinates_.push_back(subordinate); }

};


// ===================================================================================================================
// HELPERS
//



} // namespace trails::base



#endif  // __BASE_ENTITIES__

