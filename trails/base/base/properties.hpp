/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: base
 * \file properties.hpp
 *
 * Properties include.
 *
 * \author Erich Gatejen
 */

#pragma once
#ifndef __BASE_PROPERTIES__
#define __BASE_PROPERTIES__

// ===================================================================================================================
// INCLUDES
//

#include <string>
#include <variant>
#include <map>

#include <cereal/archives/portable_binary.hpp>
#include <cereal/types/variant.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/tuple.hpp>

#include "base.hpp"

namespace trails::base {

// ===================================================================================================================
// PROPERTIES
//

enum class PropertiesNormalType
{
    INT32_T,
    FLOAT,
    STRING
};

using trails_property_name = std::string;
using trails_property_value = std::variant<int32_t,float,std::string>;
using trails_property = std::tuple<trails_property_name,trails_property_value>;
using trails_properties = std::map<trails_property_name,trails_property>;

struct Properties
{
    C_FRIEND;
    trails_properties properties_;

public:

    C_SERIALIZE(properties_);

    trails_property & operator[](trails_property_name);

    trails_properties::iterator begin() { return properties_.begin(); }
    trails_properties::iterator end()   { return properties_.end(); }

    bool has(const trails_property_name & name);
    bool remove(const trails_property_name & name);

    template <typename R> std::pair<bool,R> get(const trails_property_name & name);
    template <> std::pair<bool,int32_t> get<int32_t>(const trails_property_name & name);
    template <> std::pair<bool,float> get<float>(const trails_property_name & name);
    template <> std::pair<bool,std::string> get<std::string>(const trails_property_name & name);

};

template <typename R> R get(const trails_property_value & value);
template <> int32_t get<int32_t>(const trails_property_value & value);
template <> float get<float>(const trails_property_value & value);
template <> std::string get<std::string>(const trails_property_value & value);


// ===================================================================================================================
// HELPERS
//



} // namespace trails::base



#endif  // __BASE_PROPERTIES__

