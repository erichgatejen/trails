/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: base
 * \file tree.hpp
 *
 * Hierarchical tree structure.
 *
 * \author Erich Gatejen
 */
#pragma once
#ifndef __BASE_DATA_TREE__
#define __BASE_DATA_TREE__

#include <vector>
#include <variant>
#include <optional>
#include <type_traits>

#include "base.hpp"
#include "platform_data.hpp"

namespace trails::base {

// ===================================================================================================================
// HIERARCHICAL TREE
//

using hierarchical_tree_name_element = std::string;
using hierarchical_tree_name  = std::vector<std::string>;
#define hierarchical_tree_node_list std::vector<std::shared_ptr<HierarchicalTreeNode<T>>>

template < typename T>
struct HierarchicalTreeItem
{
    hierarchical_tree_name   name;
    T   value;
    HierarchicalTreeItem() = default;
    HierarchicalTreeItem(hierarchical_tree_name name, T value) : name(std::move(name)), value(value) {};
};

std::string hierarchical_tree_name_to_string(hierarchical_tree_name & name);

#define hierarchical_tree_item_list(_T_) std::vector<HierarchicalTreeItem<_T_>>

template < typename T>
struct HierarchicalTreeNode
{
    hierarchical_tree_name_element   name;
    T   value;
    hierarchical_tree_node_list      children;

    std::shared_ptr<trails::base::HierarchicalTreeNode<int>> find_child_node(const hierarchical_tree_name_element &node_name)
    {
        std::shared_ptr<trails::base::HierarchicalTreeNode<int>> result;
        for (auto child : children)
        {
            if (child->name == node_name)
            {
                result = child;
                break;
            }
        }
        return result;
    }

};

template <typename T>
class HierarchicalTree
{
    std::shared_ptr<HierarchicalTreeNode<T>>   root;

    void find_and_record_leafs(hierarchical_tree_item_list(T) & result, std::shared_ptr<HierarchicalTreeNode<T>> current_node,
            std::vector<hierarchical_tree_name_element> & current_name_list)
    {
        for (auto child : current_node->children)
        {
            current_name_list.push_back(child->name);
            if (child->children.size() == 0)
            {
                result.push_back(HierarchicalTreeItem<T>(current_name_list,child->value));
            }
            else
            {
                find_and_record_leafs(result, child, current_name_list);
            }
            current_name_list.pop_back();
        }
    }

public:

    HierarchicalTree() : root(std::make_shared<HierarchicalTreeNode<T>>()) {};
    ~HierarchicalTree() = default;

    void add(hierarchical_tree_name & full_name, T value)
    {

        std::shared_ptr<HierarchicalTreeNode<T>> current =  root;
        bool found = false;
        for (auto & name : full_name )
        {
            for(auto child : current->children)
            {
                if (child->name == name)
                {
                    current = child;
                    found = true;
                    break;
                }
            }

            if (found)
            {
                // Existing node
                found = false;
            }
            else
            {
                // New node
                auto new_node = std::make_shared<HierarchicalTreeNode<T>>();
                new_node->name = name;
                new_node->value = value;
                current->children.push_back(new_node);
                current = current->children.at(current->children.size() - 1);
            }

        }
    }

    void add(hierarchical_tree_name && full_name, T value)
    {
        add(full_name, value);
    }

    hierarchical_tree_item_list(T) get_leafs()
    {
        hierarchical_tree_item_list(T) result;
        std::vector<hierarchical_tree_name_element> current_name_list;
        find_and_record_leafs(result, root, current_name_list);
        return result;
    }

};

} // namespace trails::base

#endif  // __BASE_DATA_TREE__

