/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: base
 * \file platform_data.hpp
 *
 * Platform specific data.
 *
 * \author Erich Gatejen
 */

#pragma once
#ifndef __BASE_PLATFORM_DATA__
#define __BASE_PLATFORM_DATA__

#include <cstdint>

// ===================================================================================================================
// TYPES
//

namespace trails::base {

#if defined(_WIN32)
    #ifdef defined(__x86_64__) || defined(_M_X64)
     // Supported

    #else
        #error "Win32 32-bit not supported."
    #endif

#elif defined(TARGET_OS_MAC) || defined(__APPLE__)
    // Supported

#elif defined(__linux__) && (__x86_64__)
    #error "Platform not yet implemented."

#else
    #error "Platform not yet supported."

#endif



} // namespace trails::base

#endif  // __BASE_PLATFORM_DATA__

