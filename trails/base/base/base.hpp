/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: base
 * \file base.h
 *
 * Base include.
 *
 * \author Erich Gatejen
 */

#pragma once
#ifndef __BASE__
#define __BASE__

// ===================================================================================================================
// INCLUDES
//

#include "platform_data.hpp"

namespace trails::base {

// ===================================================================================================================
// TYPES
//

using trails_id = u_int32_t;

const trails_id starting_id = 0;

using result = std::pair<bool,std::string>;

// ===================================================================================================================
// DATA
//



// ===================================================================================================================
// HELPERS
//

/*! \function void sleep_ms(unsigned long milliseconds)
 *  \brief Portable sleep in milliseconds
 *  In general sleeps are a bad idea.
 *  \p milliseconds time to sleep.
 */
void sleep_ms(unsigned long milliseconds);

template <class ... V>
struct expand_visitor : V... {
    expand_visitor(V const & ... v) : V{v}...
    {}

    using V::operator()...;
};


} // namespace trails::base


// ===================================================================================================================
// CEREAL / RAPIDJSON out of namespace.
//

#define C_FRIEND friend class cereal::access;
#define C_SERIALIZE(...) \
    template <class Archive> \
    void serialize( Archive & ar ) { ar( __VA_ARGS__ ); }


#endif  // __BASE__

