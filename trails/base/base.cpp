/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: base
 * \file base.cpp
 *
 * Base implementations.
 *
 * \author Erich Gatejen
 */

#include <ostream>
#include <chrono>
#include <thread>
#include <string>
#include <sstream>

#include "base/base.hpp"
#include "base/tree.hpp"

// ===================================================================================================================
// GENERAL DATA
//

std::string trails::base::hierarchical_tree_name_to_string(trails::base::hierarchical_tree_name & name)
{
    std::stringstream accum;
    for (int index  = 1; index < name.size(); index++)
    {
        accum << ':';
        accum << name[index].substr(1, name[index].size() - 2);
    }
    return accum.str();
}

// ===================================================================================================================
// HELPERS
//

void trails::base::sleep_ms(unsigned long milliseconds)
{
#ifdef _WIN32
    Sleep(milliseconds);
#else
    std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds));
#endif
}
