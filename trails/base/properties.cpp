/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: base
 * \file properties.cpp
 *
 * Properties implementations.
 *
 * \author Erich Gatejen
 */

#include "base/properties.hpp"


// =========================================================== ========================================================
// PROPERTIES
//

trails::base::trails_property & trails::base::Properties::operator[](trails_property_name name)
{
    return properties_[name];
}

bool trails::base::Properties::has(const trails::base::trails_property_name & name)
{
    return properties_.count(name);
}

bool trails::base::Properties::remove(const trails::base::trails_property_name & name)
{
    return properties_.erase(name);
}

// TODO compact these
template <> std::pair<bool,int32_t> trails::base::Properties::get<int32_t>(const trails_property_name & name)
{
    if (properties_.contains(name))
    {
        return { true,
            std::visit(
                // std::variant<int32_t,float,std::string>
                expand_visitor(
                    [](int32_t i)->int32_t { return i; },
                    [](float i)->int32_t { return (int32_t)roundf(i); },
                    [](std::string i)->int32_t { return std::atoi(i.c_str()); }
                ),
                std::get<1>(properties_[name])
            ) };
    }
    else
    {
        return {false, 0};
    }
}

template <> std::pair<bool,float> trails::base::Properties::get<float>(const trails_property_name & name)
{
    if (properties_.contains(name))
    {
        return { true,
                 std::visit(
                     // std::variant<int32_t,float,std::string>
                     expand_visitor(
                         [](int32_t i)->float { return (float)i; },
                         [](float i)->float { return i; },
                         [](std::string i)->float { return std::atof(i.c_str()); }
                     ),
                     std::get<1>(properties_[name])
                 ) };
    }
    else
    {
        return {false, 0};
    }
}

template <> std::pair<bool,std::string> trails::base::Properties::get<std::string>(const trails_property_name & name)
{
    if (properties_.contains(name))
    {
        return { true,
                 std::visit(
                     // std::variant<int32_t,float,std::string>
                     expand_visitor(
                         [](int32_t i)->std::string { return std::to_string(i); },
                         [](float i)->std::string { return std::to_string(i); },
                         [](std::string i)->std::string { return i; }
                     ),
                     std::get<1>(properties_[name])
                 ) };
    }
    else
    {
        return {false, 0};
    }
}

template <> int32_t trails::base::get<int32_t>(const trails_property_value & value)
{
    return std::visit(
                 // std::variant<int32_t,float,std::string>
                 expand_visitor(
                     [](int32_t i)->int32_t { return i; },
                     [](float i)->int32_t { return (int32_t)roundf(i); },
                     [](std::string i)->int32_t { return std::atoi(i.c_str()); }
                 ),
                 value
             );
}

template <> float trails::base::get<float>(const trails_property_value & value)
{
    return std::visit(
                 // std::variant<int32_t,float,std::string>
                 expand_visitor(
                     [](int32_t i)->float { return (float)i; },
                     [](float i)->float { return i; },
                     [](std::string i)->float { return std::atof(i.c_str()); }
                 ),
                 value
             );
}

template <> std::string trails::base::get<std::string>(const trails_property_value & value)
{
    return std::visit(
                 // std::variant<int32_t,float,std::string>
                 expand_visitor(
                     [](int32_t i)->std::string { return std::to_string(i); },
                     [](float i)->std::string { return std::to_string(i); },
                     [](std::string i)->std::string { return i; }
                 ),
                 value
             );
}
