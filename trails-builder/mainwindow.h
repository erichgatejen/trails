/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c_) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: trails-builder
 * \file mainwindow.h
 *
 * Main window.
 *
 * \author Erich Gatejen
 */

#pragma once
#ifndef __BUILDER_MAINWINDOW__
#define __BUILDER_MAINWINDOW__

#include <QMainWindow>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

#include "config.h"
#include "mainwindow.h"
#include "maincontext.h"
#include "menubar.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

    MainContext c;

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void setup_ui();
    void retranslate_ui();

private slots:

private:

};

#endif // __BUILDER_MAINWINDOW__
