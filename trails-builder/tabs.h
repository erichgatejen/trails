/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c_) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: trails-builder
 * \file tabs.h
 *
 * Main window tabs.
 *
 * \author Erich Gatejen
 */

#pragma once
#ifndef __BUILDER_TABS__
#define __BUILDER_TABS__

#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <QtWidgets/QListWidget>

#include "config.h"
#include "maincontext.h"
#include "project.h"
#include "properties.h"

class ProjectTab : public QWidget
{
    Q_OBJECT
    MainContext * c_;

    FreeProperties * free_properties_;
    ProjectProperties * project_properies_;

public:
    ProjectTab(MainContext * context, QWidget *parent = nullptr);
    void render_all();

};

class EntityTab : public QWidget
{
    Q_OBJECT
    MainContext * c_;

    QListWidget * entity_list_;
    QWidget * entity_info_;

public:
    EntityTab(MainContext * context, QWidget *parent = nullptr);
    void render_all();
};

class FlowTab : public QWidget
{
    Q_OBJECT
    MainContext * c_;

public:
    FlowTab(MainContext * context, QWidget *parent = nullptr);
    void render_all();
};

#endif // __BUILDER_TABS__
