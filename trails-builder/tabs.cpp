/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c_) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: trails-builder
 * \file tabs.cpp
 *
 * Main tabs..
 *
 * \author Erich Gatejen
 */

#include "tabs.h"

#include <QtWidgets>

ProjectTab::ProjectTab(MainContext * context, QWidget * parent)
{
    c_ = context;
    setObjectName(QString::fromUtf8("project_tab"));

    auto layout = new QVBoxLayout();
    layout->setObjectName(QString::fromUtf8("project_tab_horizontal_layout"));
    layout->setContentsMargins(0, 0, 2, 2);

    auto up_label = new QLabel("User Properties", this);
    up_label->setAlignment(Qt::AlignHCenter);
    layout->addWidget(up_label);

    free_properties_ = new FreeProperties(this, context);
    layout->addWidget(free_properties_);

    layout->setAlignment(Qt::AlignHCenter);
    setLayout(layout);
}

void ProjectTab::render_all()
{
    free_properties_->render_all();
//    project_properies_->render_all();
}

EntityTab::EntityTab(MainContext * context, QWidget * parent)
{
    c_ = context;
    setObjectName(QString::fromUtf8("entity_tab"));

    entity_list_ = new QListWidget(this);
    entity_list_->setMaximumSize(QSize(TB__ENTITY_TABLE_X_SIZE, 16777215));

    entity_info_ = new QWidget(this);

    auto layout = new QHBoxLayout();
    layout->setContentsMargins(2, 2, 2, 2);
    layout->addWidget(entity_list_);
    layout->addWidget(entity_info_);
    setLayout(layout);

}

void EntityTab::render_all()
{

}


FlowTab::FlowTab(MainContext * context, QWidget * parent)
{
    c_ = context;
    setObjectName(QString::fromUtf8("flow_tab"));

    auto layout = new QHBoxLayout();
    layout->setContentsMargins(2, 2, 2, 2);
    setLayout(layout);
}

void FlowTab::render_all()
{

}
