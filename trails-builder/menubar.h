/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c_) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: trails-builder
 * \file mainwindow.h
 *
 * Main window.
 *
 * \author Erich Gatejen
 */

#pragma once
#ifndef __BUILDER_MENUBAR__
#define __BUILDER_MENUBAR__

#include <QMainWindow>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

#include "config.h"
#include "maincontext.h"

class MainMenuBarManager : public QObject
{
    Q_OBJECT

    QMenu * menu_file;
    QAction * action_file_new;
    QAction * action_file_open;
    QAction * action_file_save;
    QAction * action_file_quit;

    bool maybe_save();

private slots:
    void slot_file_new();
    void slot_file_open();
    void slot_file_save();
    void slot_file_quit();

public:
    MainContext * c;

    MainMenuBarManager();

    void setup(MainContext * context);
    void setup_actions();
    void setup_menus();

    void save_allowed(const bool is_allowed);
};

#endif // __BUILDER_MENUBAR__
