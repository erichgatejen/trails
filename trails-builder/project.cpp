/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c_) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: trails-builder
 * \file project.cpp
 *
 * Project.
 *
 * \author Erich Gatejen
 */

#include <fstream>

#include <cereal/archives/portable_binary.hpp>

#include "config.h"
#include "maincontext.h"
#include "project.h"

TrailsProject::TrailsProject()
{
    data_ = std::make_shared<TrailsProjectData>();
}

trails::base::result  TrailsProject::new_project(const std::string project_root_path)
{
    project_root_path_ = project_root_path;
    data_ = std::make_shared<TrailsProjectData>();
    trails::game::trails_load_default_project_properties(data_->project_properties);

    return {true, ""};
}

trails::base::result  TrailsProject::load_project(const std::string project_root_path)
{
    project_root_path_ = project_root_path;

    try
    {
        std::ifstream is(project_root_path + '/' + TB__PROPERTIES_FILE_NAME, std::ios::binary);
        if (is.good())
        {
            data_ = std::make_shared<TrailsProjectData>();
            cereal::PortableBinaryInputArchive pbia(is);
            pbia(*data_);
        }
        else
        {
            return {false, "Could not open project_properties file."};
        }

    }
    catch (std::exception & e)
    {
        return {false, e.what()};
    }

    return { true, ""};
}

trails::base::result TrailsProject::save_project()
{
    try
    {
        std::ofstream os(project_root_path_ + '/' + TB__PROPERTIES_FILE_NAME, std::ios::binary);
        if (os.good())
        {
            cereal::PortableBinaryOutputArchive pboa(os);
            pboa(*data_);
        }
        else
        {
            return {false, "Could not open project_properties file to save."};
        }

    }
    catch (std::exception & e)
    {
        return {false, e.what()};
    }

    return { true, ""};
}



