/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c_) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: trails-builder
 * \file common.h
 *
 * Common widgets and stuff.
 *
 * \author Erich Gatejen
 */

#pragma once
#ifndef __BUILDER_COMMON__
#define __BUILDER_COMMON__

#include <QtWidgets>

#include "config.h"
#include "maincontext.h"

class ProblemDialog : public QDialog
{
    Q_OBJECT

    QPushButton * ok_button_;
    QPlainTextEdit * message_;
    QPlainTextEdit * reason_;

private slots:
    void ok_clicked();


public:
    ProblemDialog(QWidget *parent = nullptr);
    inline void set_message(const QString & message) { message_->setPlainText(message); }
    inline void set_reason(const QString & reason) { reason_->setPlainText(reason); }
};

void trails_show_problem_dialog(MainContext * c, const QString & message, const QString & reason);

#endif // __BUILDER_MENUBAR__
