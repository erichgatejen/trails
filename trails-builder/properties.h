/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c_) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: trails-builder
 * \file project_properties.h
 *
 * Various project_properties tables.
 *
 * \author Erich Gatejen
 */

#pragma once
#ifndef __BUILDER_PROPERTIES__
#define __BUILDER_PROPERTIES__

#include <QDialog>
#include <QLineEdit>

#include "base/properties.hpp"
#include "game/game_properties.h"

#include "maincontext.h"

// =========================================================================================
// = PROPERTIES BASE

class PropertiesBase : public QWidget
{
    Q_OBJECT

    QTableWidget * prop_table_;
    QPushButton * delete_button_;
    QPushButton * edit_button_;

protected:
    MainContext * context_;

public:
    PropertiesBase(QWidget * parent, MainContext * context);

    virtual trails::base::Properties & get_properties() = 0;
    inline MainContext * get_context() { return context_; };

    void render_all();
    void render_add(const trails::base::trails_property_name & name);
    void render_changed(const int row, const trails::base::trails_property_name & name);
    void render_remove(const int row);

    virtual std::tuple<bool, trails::base::trails_property_name, trails::base::trails_property_value>
        edit(const trails::base::trails_property_name & name, const trails::base::trails_property_value & value) = 0;
    virtual std::tuple<bool, trails::base::trails_property_name, trails::base::trails_property_value> add() = 0;

private slots:
    void add_clicked();
    void edit_clicked();
    void delete_clicked();
    void cell_clicked(int row, int col);

};


// =========================================================================================
// = FREE PROPERTIES

class FreeProperties;
class FreePropertyDialog : public QDialog
{
Q_OBJECT

    QLineEdit * name_;
    QLineEdit * value_;

public:
    FreePropertyDialog(QWidget *parent = nullptr);
    std::tuple<bool, trails::base::trails_property_name, trails::base::trails_property_value> run(
        const trails::base::trails_property_name name, const std::string value);
};


class FreeProperties : public PropertiesBase
{
    Q_OBJECT
    FreePropertyDialog * pdialog;

public slots:

public:
    FreeProperties(QWidget * parent, MainContext * context);
    virtual trails::base::Properties & get_properties() override;

    virtual std::tuple<bool, trails::base::trails_property_name, trails::base::trails_property_value>
        edit(const trails::base::trails_property_name & name, const trails::base::trails_property_value & value) override;
    virtual std::tuple<bool, trails::base::trails_property_name, trails::base::trails_property_value> add() override;
};


// =========================================================================================
// = PROJECT PROPERTIES

class ProjectProperties : public PropertiesBase
{
    Q_OBJECT

public slots:

public:
    virtual trails::base::Properties & get_properties() override;
};

#endif // __BUILDER_PROPERTIES__
