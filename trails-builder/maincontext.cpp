/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c_) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: trails-builder
 * \file maincontext.cpp
 *
 * Managers and helpers for context.
 *
 * \author Erich Gatejen
 */

#include "maincontext.h"
#include "tabs.h"
#include "menubar.h"

MainContext::MainContext()
{
    // Empty project
    project = std::make_shared<TrailsProject>();

    main_menu_bar_manager = std::make_shared<MainMenuBarManager>();
}

void MainContext::render_all()
{
    // New or loaded project.
    project_tab->render_all();
    entity_tab->render_all();
    flow_tab->render_all();

}

void MainContext::modified()
{
    project->modified(true);
    main_menu_bar_manager->save_allowed(true);
}

void MainContext::saved()
{
    project->modified(false);
    main_menu_bar_manager->save_allowed(false);
}

void tb_tab_enable(MainContext * context, const bool enabled)
{
    context->tab_widget->setTabEnabled(TB__TAB_INDEX__PROJECT, enabled);
    context->tab_widget->setTabEnabled(TB__TAB_INDEX__ENTITIES, enabled);
    context->tab_widget->setTabEnabled(TB__TAB_INDEX__FLOW, enabled);
}


