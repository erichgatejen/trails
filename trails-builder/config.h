/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c_) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: trails-builder
 * \file config.h
 *
 * Code configurations.
 *
 * \author Erich Gatejen
 */

#pragma once
#ifndef __BUILDER_CONFIG__
#define __BUILDER_CONFIG__

#define TB__ENTITY_TABLE_X_SIZE   200

#define TB__TAB_INDEX__PROJECT      0
#define TB__TAB_INDEX__ENTITIES     1
#define TB__TAB_INDEX__FLOW         2

#define TB__PROJECT_FILE_NAME       "trails.project"
#define TB__PROPERTIES_FILE_NAME    "trails.prop"


#endif // __BUILDER_CONFIG__
