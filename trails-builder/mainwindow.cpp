/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c_) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: trails-builder
 * \file mainwindow.cpp
 *
 * Main window.
 *
 * \author Erich Gatejen
 */

#include "mainwindow.h"
#include "tabs.h"

MainWindow::MainWindow(QWidget * parent)
    : QMainWindow(parent)
{
    c.main_window = this;
    setup_ui();

}

MainWindow::~MainWindow()
{
}

void MainWindow::setup_ui()
{
    if (this->objectName().isEmpty())
        this->setObjectName(QString::fromUtf8("MainWindow"));
    this->resize(800, 600);

    c.central_widget = new QWidget(this);
    c.central_widget->setObjectName(QString::fromUtf8("central_widget"));
    c.central_widget_vertical_layout = new QVBoxLayout(c.central_widget);
    c.central_widget_vertical_layout->setObjectName(QString::fromUtf8("central_widget_vertical_layout"));
    c.central_widget_vertical_layout->setContentsMargins(2, 2, 2, 2);

    // -- TABS ---------------------------------------------------------------------
    c.tab_widget = new QTabWidget(c.central_widget);
    c.tab_widget->setObjectName(QString::fromUtf8("tab_widget"));

    c.project_tab = new ProjectTab(& c);
    c.tab_widget->addTab(c.project_tab, tr("Project"));
    c.entity_tab = new EntityTab(& c);
    c.tab_widget->addTab(c.entity_tab, tr("Entities"));
    c.flow_tab = new FlowTab(& c);
    c.tab_widget->addTab(c.flow_tab, tr("Flow"));

    tb_tab_enable(& c, false);  //  All disables at load.

    // -- FINALIZE CENTRAL --------------------------------------------------------------
    c.central_widget_vertical_layout->addWidget(c.tab_widget);
    this->setCentralWidget(c.central_widget);

    // -- MENU BAR ---------------------------------------------------------------------
    c.main_qmenubar = new QMenuBar(this);
    c.main_qmenubar->setObjectName(QString::fromUtf8("main_qmenubar"));
    c.main_qmenubar->setGeometry(QRect(0, 0, 800, 22));
    c.main_menu_bar_manager->setup(&c);
    this->setMenuBar(c.main_qmenubar);

    // -- FLOW TAB ---------------------------------------------------------------------
    c.statusbar = new QStatusBar(this);
    c.statusbar->setObjectName(QString::fromUtf8("statusbar"));
    this->setStatusBar(c.statusbar);

    // -- OVERHEAD MainWindow
    retranslate_ui();
    c.tab_widget->setCurrentIndex(0);
    QMetaObject::connectSlotsByName(this);

}

void MainWindow::retranslate_ui()
{
    this->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
    c.tab_widget->setTabText(c.tab_widget->indexOf(c.entity_tab), QCoreApplication::translate("MainWindow", "Nodes", nullptr));
    c.tab_widget->setTabText(c.tab_widget->indexOf(c.flow_tab), QCoreApplication::translate("MainWindow", "Flow", nullptr));
}
