/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c_) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: trails-builder
 * \file project.h
 *
 * Project information.
 *
 * \author Erich Gatejen
 */

#pragma once
#ifndef __BUILDER_PROJECT__
#define __BUILDER_PROJECT__

#include "base/properties.hpp"
#include "game/game_properties.h"

struct TrailsProjectData
{
    trails::base::Properties    project_properties;
    trails::base::Properties    free_properties;

    C_SERIALIZE(project_properties, free_properties);
};

class TrailsProject
{
    C_FRIEND

    // Project meta
    std::string project_root_path_;
    bool is_modified_ = false;
    std::shared_ptr<TrailsProjectData>  data_;

public:
    TrailsProject();
    virtual ~TrailsProject() = default;

    trails::base::result new_project(const std::string project_root_path);
    trails::base::result load_project(const std::string project_root_path);
    trails::base::result save_project();
    //void discard_project();

    inline void modified(bool saved = false) { if (saved) is_modified_ = false; else is_modified_ = true; }
    inline bool is_modified() { return is_modified_; }
    inline std::shared_ptr<TrailsProjectData> get_data() { return data_; }


};


#endif // __BUILDER_PROJECT__
