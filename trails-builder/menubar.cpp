/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c_) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: trails-builder
 * \file main_qmenubar.cpp
 *
 * Menu bar manager.
 *
 * \author Erich Gatejen
 */

#include <filesystem>

#include "common.h"
#include "menubar.h"
#include "project.h"

#include <QtWidgets>
#include <QAction>
#include <QMessageBox>

MainMenuBarManager::MainMenuBarManager()
{

}

void MainMenuBarManager::setup(MainContext * context)
{
    c = context;
    setup_actions();
    setup_menus();

    // I guess QT5 addressed this.
    //#if defined(TARGET_OS_MAC) || defined(__APPLE__)
        // Native menu bars are a giant pain in OSX
        //c->main_qmenubar->setNativeMenuBar(false);
    //#endif
}

void MainMenuBarManager::setup_actions()
{
    action_file_new = new QAction(tr("&New"), this);
    action_file_new->setShortcuts(QKeySequence::New);
    action_file_new->setStatusTip(tr("Create a new file"));
    c->main_window->connect(action_file_new, &QAction::triggered, this, &MainMenuBarManager::slot_file_new);

    action_file_open = new QAction(tr("&Open..."), this);
    action_file_open->setShortcuts(QKeySequence::Open);
    action_file_open->setStatusTip(tr("Open an existing file"));
    c->main_window->connect(action_file_open, &QAction::triggered, this, &MainMenuBarManager::slot_file_open);

    action_file_save = new QAction(tr("&Save"), this);
    action_file_save->setShortcuts(QKeySequence::Save);
    action_file_save->setStatusTip(tr("Save the document to disk"));
    action_file_save->setEnabled(false);
    c->main_window->connect(action_file_save, &QAction::triggered, this, &MainMenuBarManager::slot_file_save);

    action_file_quit = new QAction(tr("&Quit"), this);
    action_file_quit->setShortcuts(QKeySequence::Quit);
    action_file_quit->setStatusTip(tr("Quit application"));
    c->main_window->connect(action_file_quit, &QAction::triggered, this, &MainMenuBarManager::slot_file_quit);
}

void MainMenuBarManager::setup_menus()
{
    menu_file = c->main_qmenubar->addMenu(tr("&File"));
    menu_file->addAction(action_file_new);
    menu_file->addAction(action_file_open);
    menu_file->addAction(action_file_save);
    menu_file->addSeparator();
    menu_file->addAction(action_file_quit);
}

void MainMenuBarManager::save_allowed(const bool is_allowed)
{
    if (is_allowed)
        action_file_save->setDisabled(false);
    else
        action_file_save->setDisabled(true);
}

// ==========================================================================================
// = SLOTS

class ProjectDirectoryFilterProxyModel : public QSortFilterProxyModel
{
protected:
    virtual bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const
    {
        QFileSystemModel* fileModel = qobject_cast<QFileSystemModel*>(sourceModel());
        return (fileModel!=NULL && fileModel->isDir(sourceModel()->index(sourceRow, 0, sourceParent))) || QSortFilterProxyModel::filterAcceptsRow(sourceRow, sourceParent);
    }
};

std::pair<bool,QString> trails_get_directory(MainContext * c)
{
    bool accpted = false;
    QString value;

    QFileDialog dialog(c->central_widget);
    dialog.setFileMode(QFileDialog::Directory);
    dialog.setOption(QFileDialog::ShowDirsOnly);
    dialog.setDirectory(QDir::home());
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptOpen);
    dialog.setOption(QFileDialog::DontUseNativeDialog);
    ProjectDirectoryFilterProxyModel * proxyModel = new ProjectDirectoryFilterProxyModel;
    dialog.setProxyModel(proxyModel);

    if (dialog.exec() == QDialog::Accepted)
    {
        value = dialog.selectedFiles().first();
        accpted = true;
    }
    return {accpted, value};
}

void MainMenuBarManager::slot_file_new()
{
    if (! maybe_save()) return;

    auto [accepted, path] = trails_get_directory(c);
    auto path_s = path.toStdString();
    if (accepted)
    {
        QDir selected_dir(path);
        if (selected_dir.count() > 2)
        {
            const QMessageBox::StandardButton result
                = QMessageBox::warning(c->central_widget,
                                       tr("New Project"),
                                       tr("You have selected a project directory that isn't empty.\n"
                                          "This will overwrite all filed that are there.  Are you sure?"),
                                       QMessageBox::Yes | QMessageBox::Cancel);
            switch (result)
            {
            case QMessageBox::Cancel:
                return;
            case QMessageBox::Yes:
                try
                {
                    for (const auto& entry : std::filesystem::directory_iterator(path_s))
                    {
                        std::filesystem::remove_all(entry.path());
                    }
                }
                catch(std::runtime_error & e)
                {
                    trails_show_problem_dialog(c, tr("Could not open Project."), tr("Could not remove previous files from new project location.\n"
                                                                                    "Please make sure they can be deleted."));
                    return;
                }
                break;

            default:
                assert(false);
            }
        }

        c->project = std::make_shared<TrailsProject>();
        auto [success, message] = c->project->new_project(path_s);
        if (success)
        {
            tb_tab_enable(c, true);
            c->saved();
            c->render_all();
        }
        else
        {
            trails_show_problem_dialog(c, tr("Could create a new project."), tr(message.c_str()));
        }

    }
}

void MainMenuBarManager::slot_file_open()
{
    if (! maybe_save()) return;

    auto [accepted, path] = trails_get_directory(c);
    if (accepted)
    {
        c->project = std::make_shared<TrailsProject>();
        auto [success, message] = c->project->load_project(path.toStdString());
        if (success)
        {
            tb_tab_enable(c, true);
            c->render_all();
            c->saved();
        }
        else
        {
            trails_show_problem_dialog(c, tr("Could not open Project."), tr(message.c_str()));
        }
    }
}

void MainMenuBarManager::slot_file_save()
{
    if (c->project == nullptr)
    {
        trails_show_problem_dialog(c, tr("No project is loaded."), tr(""));
    }
    else
    {
        auto [success, message] = c->project->save_project();
        if (success)
        {
            c->saved();
        }
        else {
            trails_show_problem_dialog(c, tr("Could not open Project."), tr(message.c_str()));
        }
    }
}

void MainMenuBarManager::slot_file_quit()
{
    if (maybe_save()) QApplication::quit();
}

// ==========================================================================================
// = OTHER

bool MainMenuBarManager::maybe_save()
{
    if ((c->project !=nullptr) && (c->project->is_modified()))
    {
        const QMessageBox::StandardButton result
            = QMessageBox::warning(c->central_widget,
                                   tr("Application"),
                                   tr("The project has been modified.\n"
                                      "Do you want to save your changes?"),
                                   QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        switch (result)
        {
        case QMessageBox::Save:
            c->project->save_project();
        case QMessageBox::Cancel:
            return false;
        default:
            break;
        }
        return true;
    }
    return true;
}


