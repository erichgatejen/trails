/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c_) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: trails-builder
 * \file common.cpp
 *
 * Common elements.
 *
 * \author Erich Gatejen
 */

#include "maincontext.h"
#include "common.h"

ProblemDialog::ProblemDialog(QWidget *parent)
    : QDialog(parent)
{
    auto problem_label = new QLabel(tr("Problem encountered."));
    ok_button_ = new QPushButton(tr("OK"));

    message_ = new QPlainTextEdit(this);
    message_->setReadOnly(true);
    message_->setPlainText(tr("Unknown problem"));

    reason_ = new QPlainTextEdit(this);
    reason_->setReadOnly(true);
    reason_->setPlainText(tr("Unknown problem"));

    auto layout = new QVBoxLayout;
    layout->addWidget(message_);
    layout->addWidget(reason_);
    layout->addWidget(ok_button_);

    setLayout(layout);
    setWindowTitle(tr("Problem"));
    connect(ok_button_, &QPushButton::clicked, this, & ProblemDialog::ok_clicked);
    connect(ok_button_, &QPushButton::clicked, this, &ProblemDialog::accept);
}

void ProblemDialog::ok_clicked()
{
    hide();
}

void trails_show_problem_dialog(MainContext * c, const QString & message, const QString & reason)
{
    if (c->problem_dialog == nullptr)
    {
        c->problem_dialog = new ProblemDialog(c->central_widget);
    }
    c->problem_dialog->set_message(message);
    c->problem_dialog->set_reason(reason);
    c->problem_dialog->show();
}
