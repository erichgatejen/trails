/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c_) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: trails-builder
 * \file mainwindow.h
 *
 * Main window.
 *
 * \author Erich Gatejen
 */

#pragma once
#ifndef __BUILDER_MAINCONTEXT__
#define __BUILDER_MAINCONTEXT__

#include <QMainWindow>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

#include "config.h"
class ProjectTab;
class EntityTab;
class FlowTab;
class TrailsProject;
class ProblemDialog;
class MainMenuBarManager;

struct MainContext
{
    // ====================================================================
    // = MAIN GUI
    QMainWindow * main_window;

    QWidget * central_widget;
    QVBoxLayout * central_widget_vertical_layout;

    QTabWidget * tab_widget;
    ProjectTab * project_tab;
    EntityTab * entity_tab;
    FlowTab * flow_tab;

    QMenuBar * main_qmenubar;
    QStatusBar * statusbar;

    // ====================================================================
    // = ALTERNATE GUI

    ProblemDialog * problem_dialog = nullptr;       // Not pre-initialized.

    // ====================================================================
    // = MANAGERS

    std::shared_ptr<MainMenuBarManager> main_menu_bar_manager;

    // ====================================================================
    // = SERVICES
    void render_all();


    // ====================================================================
    // = APPLICATION
    std::shared_ptr<TrailsProject>  project;

    // ====================================================================
    // = OTHER
    MainContext();
    virtual ~MainContext() = default;

    void modified();
    void saved();

};

// =======================================================================================
// = Managers

void tb_tab_enable(MainContext * context, const bool enabled);

#endif // __BUILDER_MAINCONTEXT__
