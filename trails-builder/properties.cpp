/****************************************************************************************************************
 * TRAILS
 *
 * \copyright (c_) Erich P Gatejen 2021
 * ALL RIGHTS RESERVED
 * This portion of the trails project is licensed under LGPL 3
 *
 * Subsystem: trails-builder
 * \file project_properties.cpp
 *
 * Properties tables.
 *
 * \author Erich Gatejen
 */

#include <QPushButton>
#include <QLabel>
#include <QDialogButtonBox>
#include <QGroupBox>
#include <QFormLayout>
#include <QMessageBox>
#include <QDialog>
#include <QSizePolicy>

#include "config.h"
#include "maincontext.h"
#include "project.h"
#include "properties.h"

// =========================================================================================
// = PROPERTIES BASE

PropertiesBase::PropertiesBase(QWidget * parent, MainContext * context) :
    QWidget(parent), context_(context)
{
    auto layout = new QVBoxLayout();

    prop_table_ = new QTableWidget(this);
    prop_table_->setColumnCount(2);
    prop_table_->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Name")));
    prop_table_->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Value")));
    prop_table_->horizontalHeader()->setStretchLastSection(true);
    prop_table_->setColumnWidth(0, 300);
    prop_table_->setSelectionBehavior(QAbstractItemView::SelectRows);
    prop_table_->setSelectionMode(QAbstractItemView::SingleSelection);
    //prop_table_->setEditTriggers(QAbstractItemView::NoEditTriggers);
    prop_table_->setEditTriggers(QAbstractItemView::EditTrigger::DoubleClicked);
    prop_table_->setStyleSheet(
        "background-color: #FCFCFC;"
        "border: 1px solid #4181C0;"
        "color: #111111;"
        "selection-background-color: #DDDD23;"
        "selection-color: #000000;"

        "QHeaderView::section {"
        "background:#FAFAFA;"
        "color: #151515;"
        "}"
    );
    prop_table_->setEditTriggers(QAbstractItemView::NoEditTriggers);

    connect(prop_table_, SIGNAL(cellClicked(int, int)), this, SLOT(cell_clicked(int, int)));

    layout->addWidget(prop_table_);

    auto button_container = new QWidget(this);
    auto button_layout = new QHBoxLayout();

    auto add_button = new QPushButton(tr("&Add"), button_container);
    QObject::connect(add_button, SIGNAL(clicked()),this, SLOT(add_clicked()));
    button_layout->addWidget(add_button);

    edit_button_ = new QPushButton(tr("&Edit"), button_container);
    edit_button_->setDisabled(true);
    QObject::connect(edit_button_, SIGNAL(clicked()),this, SLOT(edit_clicked()));
    button_layout->addWidget(edit_button_);

    delete_button_ = new QPushButton(tr("&Delete"), button_container);
    delete_button_->setDisabled(true);
    QObject::connect(delete_button_, SIGNAL(clicked()),this, SLOT(delete_clicked()));
    button_layout->addWidget(delete_button_);

    button_container->setLayout(button_layout);
    layout->addWidget(button_container);

    setLayout(layout);
}

void trails_builder_property_table_set_row(QTableWidget * prop_table_, PropertiesBase * owner, int row, const trails::base::trails_property_name & name)
{
    auto name_item = new QTableWidgetItem( QString(name.c_str()));
    name_item->setFlags(Qt::ItemIsSelectable);
    prop_table_->setItem(row, 0, name_item);

    auto [result, value] = owner->get_properties().get<std::string>(name);
    auto value_item = new QTableWidgetItem( QString(value.c_str()) );
    value_item->setFlags(Qt::ItemIsSelectable);
    prop_table_->setItem(row, 1, value_item);
}

void PropertiesBase::render_all()
{
    int row = 0;
    prop_table_->clearContents();
    for (auto item : get_properties())
    {
        prop_table_->setRowCount(row + 1);
        trails_builder_property_table_set_row(prop_table_, this, row, item.first);
        row++;
    }
}

void PropertiesBase::render_add(const trails::base::trails_property_name & name)
{
    int row = prop_table_->rowCount();
    prop_table_->setRowCount(row + 1);
    trails_builder_property_table_set_row(prop_table_, this, row, name);
}

void PropertiesBase::render_changed(const int row, const trails::base::trails_property_name & name)
{
    prop_table_->item(row, 0)->setText(QString(name.c_str()));
    auto [result, value] = get_properties().get<std::string>(name);
    prop_table_->item(row, 1)->setText(QString(value.c_str()));
}

void PropertiesBase::render_remove(const int row)
{
    prop_table_->removeRow(row);
}

void PropertiesBase::cell_clicked(int row, int col)
{
    delete_button_->setEnabled(true);
    edit_button_->setEnabled(true);
    prop_table_->selectRow(row);
}

//void PropertiesBase::item_selection_change()
//{
//    if (prop_table_->selectedItems().empty())
//    {
//        delete_button_->setDisabled(true);
//    }
//}

void PropertiesBase::add_clicked()
{
    auto [ success, new_name, new_value ] = add();
    if (success)
    {
        render_add(new_name);
    }
}

void PropertiesBase::edit_clicked()
{
    QItemSelectionModel * item_model = prop_table_->selectionModel();
    QModelIndexList selected = item_model->selectedRows();
    if (! selected.empty())
    {
        int row = selected.first().row();
        auto name = prop_table_->item(row, 0)->text().toStdString();
        auto value = prop_table_->item(row, 1)->text().toStdString();
        auto [success, new_name, new_value] = edit(name, value);
        if (success)
        {
            render_changed(row, new_name);
        }
    }
}

void PropertiesBase::delete_clicked()
{
    QItemSelectionModel * item_model = prop_table_->selectionModel();
    QModelIndexList selected = item_model->selectedRows();
    if (! selected.empty())
    {
        int row = selected.first().row();

        get_properties().remove(prop_table_->item(row, 0)->text().toStdString());
        assert(!get_properties().has(prop_table_->item(row, 0)->text().toStdString()));
        render_remove(row);
        delete_button_->setDisabled(true);
        edit_button_->setDisabled(true);
    }
}

// =========================================================================================
// = FREE PROPERTIES

FreePropertyDialog::FreePropertyDialog(QWidget * parent)
    : QDialog(parent)
{
    auto button_box = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, this);
    connect(button_box, &QDialogButtonBox::accepted, this, &FreePropertyDialog::accept);
    connect(button_box, &QDialogButtonBox::rejected, this, &FreePropertyDialog::reject);

    auto form_group_box = new QGroupBox(tr("Property"), this);
    name_ = new QLineEdit(form_group_box);
    name_->clear();
    value_ = new QLineEdit(form_group_box);
    value_->clear();

    QFormLayout * layout = new QFormLayout;
    layout->addRow(new QLabel(tr("Name")), name_ );
    layout->addRow(new QLabel(tr("Value")), value_);
    form_group_box->setLayout(layout);

    auto dialog_layout = new QVBoxLayout(this);
    dialog_layout->addWidget(form_group_box);
    dialog_layout->addWidget(button_box);
    setLayout(dialog_layout);

}

std::tuple<bool,trails::base::trails_property_name,trails::base::trails_property_value> FreePropertyDialog::run(
    const trails::base::trails_property_name name, const std::string value)
{
    name_->setText(QString(name.c_str()));
    //value_->setText(QString(owner_->get_properties().get<std::string>(name).second.c_str()));
    value_->setText(QString(value.c_str()));

    int dialog_code = exec();
    if(dialog_code == QDialog::Accepted)
    {
        return { true, name_->text().toStdString(), value_->text().toStdString() };
    }
    return { false, "", "" };
}

// -------------

FreeProperties::FreeProperties(QWidget * parent, MainContext * context) :
    PropertiesBase(parent, context)
{
    pdialog = new FreePropertyDialog(this);
}

trails::base::Properties & FreeProperties::get_properties()
{
    return context_->project->get_data()->free_properties;
}

std::tuple<bool, trails::base::trails_property_name, trails::base::trails_property_value>
    FreeProperties::edit(const trails::base::trails_property_name & name, const trails::base::trails_property_value & value)
{
    auto [success, new_name, new_value] = pdialog->run(name, trails::base::get<std::string>(value));
    if (success)
    {
        if (new_name != name)
        {
            context_->project->get_data()->free_properties.remove(name);
        }
        context_->project->get_data()->free_properties[new_name] = { new_name, new_value };
        return { true, new_name, new_value };
    }
    return { false, "", "" };
}

std::tuple<bool, trails::base::trails_property_name, trails::base::trails_property_value> FreeProperties::add()
{
    auto [success, new_name, new_value] = pdialog->run("", "");
    if (success)
    {
        if (context_->project->get_data()->free_properties.has(new_name))
        {
            const QMessageBox::StandardButton result
                = QMessageBox::warning(this,
                                       tr("Property"),
                                       tr("There is already a property with that name.  Overwrite it?"),
                                       QMessageBox::Yes | QMessageBox::Cancel);

            if (result != QMessageBox::Yes) return { false, "", "" };
        }
        context_->project->get_data()->free_properties[new_name] = { new_name, new_value };
        return { true, new_name, new_value };
    }
    return { false, "", "" };
}

// =========================================================================================
// = PROJECT PROPERTIES

trails::base::Properties & ProjectProperties::get_properties()
{
    return context_->project->get_data()->project_properties;
}
